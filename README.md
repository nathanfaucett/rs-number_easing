number_easing
=====

number easing functions


```rust
extern crate number_easing;

use number_easing::{in_out_cubic, in_out_quad};

fn main() {
    assert_eq!(in_out_cubic(0, 100, 0), 0);
    assert_eq!(in_out_cubic(0, 100, 0.25), 6);
    assert_eq!(in_out_cubic(0, 100, 0.5), 50);
    assert_eq!(in_out_cubic(0, 100, 0.75), 93);
    assert_eq!(in_out_cubic(0, 100, 1.0), 100);

    assert_eq!(in_out_quad(0, 100, 0), 0);
    assert_eq!(in_out_quad(0, 100, 0.25), 12);
    assert_eq!(in_out_quad(0, 100, 0.5), 50);
    assert_eq!(in_out_quad(0, 100, 0.75), 87);
    assert_eq!(in_out_quad(0, 100, 1.0), 100);
}
```

#![feature(test)]

extern crate number_easing;
extern crate test;

use self::number_easing::*;
use self::test::{black_box, Bencher};

static INC: f32 = 0.0001_f32;

#[inline]
fn in_quad_native(s: isize, c: isize, t: f32) -> isize {
    ((c as f32 * t * t) + s as f32) as isize
}

#[bench]
fn in_quad_bench(b: &mut Bencher) {
    b.iter(move || {
        let mut t = 0_f32;
        let mut out = black_box(0);

        while t < 1_f32 {
            out += in_quad(0_isize, 100_isize, t);
            t += INC;
        }

        out
    })
}
#[bench]
fn in_quad_native_bench(b: &mut Bencher) {
    b.iter(move || {
        let mut t = 0_f32;
        let mut out = black_box(0);

        while t < 1_f32 {
            out += in_quad_native(0_isize, 100_isize, t);
            t += INC;
        }

        out
    })
}

#[inline]
fn in_quad_no_casting_native(s: f32, c: f32, t: f32) -> f32 {
    (c * t * t) + s
}

#[bench]
fn in_quad_no_casting_bench(b: &mut Bencher) {
    b.iter(move || {
        let mut t = 0_f32;
        let mut out = black_box(0_f32);

        while t < 1_f32 {
            out += in_quad(0_f32, 100_f32, t);
            t += INC;
        }

        out
    })
}
#[bench]
fn in_quad_no_casting_native_bench(b: &mut Bencher) {
    b.iter(move || {
        let mut t = 0_f32;
        let mut out = black_box(0_f32);

        while t < 1_f32 {
            out += in_quad_no_casting_native(0_f32, 100_f32, t);
            t += INC;
        }

        out
    })
}

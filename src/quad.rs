use number_traits::{Cast, Num, Signed};

#[inline]
pub fn in_quad<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num,
    F: Copy + Cast<T> + Num,
{
    let s = s.cast();
    let c = c.cast();
    let out = (c * t * t) + s;
    out.cast()
}

#[inline]
pub fn out_quad<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    let s = s.cast();
    let c = c.cast();

    let out = (-c * t * (t - F::from_usize(2))) + s;
    out.cast()
}

#[inline]
pub fn in_out_quad<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    let s = s.cast();
    let c = c.cast();
    let one = F::one();
    let two = F::from_usize(2);
    let t = t * two;

    let out = if t < one {
        ((c / two) * t * t) + s
    } else {
        let t = t - one;
        ((-c / two) * (t * (t - two) - one)) + s
    };

    out.cast()
}

#[test]
fn in_quad_test() {
    assert_eq!(in_quad(0, 100, 0), 0);
    assert_eq!(in_quad(0, 100, 0.5), 25);
    assert_eq!(in_quad(0, 100, 1.0), 100);
}
#[test]
fn out_quad_test() {
    assert_eq!(out_quad(0, 100, 0), 0);
    assert_eq!(out_quad(0, 100, 0.5), 75);
    assert_eq!(out_quad(0, 100, 1.0), 100);
}
#[test]
fn in_out_quad_test() {
    assert_eq!(in_out_quad(0, 100, 0), 0);
    assert_eq!(in_out_quad(0, 100, 0.25), 12);
    assert_eq!(in_out_quad(0, 100, 0.5), 50);
    assert_eq!(in_out_quad(0, 100, 0.75), 87);
    assert_eq!(in_out_quad(0, 100, 1.0), 100);
}

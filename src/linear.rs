use number_traits::{Cast, Num, Signed};

#[inline]
pub fn in_linear<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num,
    F: Copy + Cast<T> + Num,
{
    let s = s.cast();
    let c = c.cast();
    let out = (c * t) + s;
    out.cast()
}

#[inline(always)]
pub fn out_linear<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    in_linear(s, c, t)
}

#[inline(always)]
pub fn in_out_linear<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    in_linear(s, c, t)
}

#[test]
fn in_linear_test() {
    assert_eq!(in_linear(0, 100, 0), 0);
    assert_eq!(in_linear(0, 100, 0.5), 50);
    assert_eq!(in_linear(0, 100, 1.0), 100);
}
#[test]
fn out_linear_test() {
    assert_eq!(out_linear(0, 100, 0), 0);
    assert_eq!(out_linear(0, 100, 0.5), 50);
    assert_eq!(out_linear(0, 100, 1.0), 100);
}
#[test]
fn in_out_linear_test() {
    assert_eq!(in_out_linear(0, 100, 0), 0);
    assert_eq!(in_out_linear(0, 100, 0.25), 25);
    assert_eq!(in_out_linear(0, 100, 0.5), 50);
    assert_eq!(in_out_linear(0, 100, 0.75), 75);
    assert_eq!(in_out_linear(0, 100, 1.0), 100);
}

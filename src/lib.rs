#![no_std]

extern crate number_traits;

mod cubic;
mod linear;
mod quad;

pub use self::cubic::{in_cubic, in_out_cubic, out_cubic};
pub use self::linear::{in_linear, in_out_linear, out_linear};
pub use self::quad::{in_out_quad, in_quad, out_quad};

use number_traits::{Cast, Num, Signed};

#[inline]
pub fn in_cubic<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num,
    F: Copy + Cast<T> + Num,
{
    let s = s.cast();
    let c = c.cast();
    let out = (c * t * t * t) + s;
    out.cast()
}

#[inline]
pub fn out_cubic<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    let s = s.cast();
    let c = c.cast();
    let one = F::one();
    let t = t - one;
    let out = c * (t * t * t + one) + s;
    out.cast()
}

#[inline]
pub fn in_out_cubic<T, F>(s: T, c: T, t: F) -> T
where
    T: Copy + Cast<F> + Num + Signed,
    F: Copy + Cast<T> + Num + Signed,
{
    let s = s.cast();
    let c = c.cast();
    let one = F::one();
    let two = F::from_usize(2);
    let t = t * two;

    let out = if t < one {
        ((c / two) * t * t * t) + s
    } else {
        let t = t - two;
        ((c / two) * (t * t * t + two)) + s
    };

    out.cast()
}

#[test]
fn in_cubic_test() {
    assert_eq!(in_cubic(0, 100, 0), 0);
    assert_eq!(in_cubic(0, 100, 0.5), 12);
    assert_eq!(in_cubic(0, 100, 1.0), 100);
}
#[test]
fn out_cubic_test() {
    assert_eq!(out_cubic(0, 100, 0), 0);
    assert_eq!(out_cubic(0, 100, 0.5), 87);
    assert_eq!(out_cubic(0, 100, 1.0), 100);
}
#[test]
fn in_out_cubic_test() {
    assert_eq!(in_out_cubic(0, 100, 0), 0);
    assert_eq!(in_out_cubic(0, 100, 0.25), 6);
    assert_eq!(in_out_cubic(0, 100, 0.5), 50);
    assert_eq!(in_out_cubic(0, 100, 0.75), 93);
    assert_eq!(in_out_cubic(0, 100, 1.0), 100);
}
